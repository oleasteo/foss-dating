# FOSS Dating (working title)

**Work in progress. Nothing worthwhile to see here yet.**

This is a free and open source software project for online dating. It is being
designed and developed with privacy and data protection in mind.

In addition, the user should have a high degree of control. Both, regarding
their profile and regarding the filters and tunes for the discovery service.

The project is separated into its different components:
+ `web/` -- The vue based web application.

See the dedicated README.md files within the components for further details.

## White paper

Details on design decisions regarding encryption, data protection and privacy
can be found within the `white-paper/` directory.

## License

This project uses the AGPL 3.0 for its own code. See the `LICENSE.txt` file for
more information and restrictions.
