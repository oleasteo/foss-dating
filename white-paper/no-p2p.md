# Why no P2P communication

P2P communication may sound like a nice security addition at first. However,
since the p2p connection must be established through the server, it is no
concept that adds additional security.

The thought behind p2p as a security aspect may be that it protects against a
malicious or infiltrated server.
However, since the server has control over establishing the p2p connection, it
may just as easily provide false data for the connection in a way that the man
in the middle (MITM) still exists.
Thus, without a trusted source of each other's public keys, p2p connection is no
more secure from MITM-Attacks than communication via the server as gateway.

In conclusion, it makes no sense, from a security perspective, to utilize p2p
communication for this service. In contrast, it would expose the sensitive data
IP-Address to the opposing peer.
