# FOSS Dating (working title) / Web Service

A preview can be seen [via gitlab pages](https://xoria.gitlab.io/foss-dating/).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
