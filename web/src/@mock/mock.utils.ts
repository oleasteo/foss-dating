import { capitalCase } from "@/utils/string";
import { identity } from "@/utils/function";

const WORDS = [
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Venenatis a condimentum vitae sapien. Nisi scelerisque eu ultrices vitae auctor. Massa tempor nec feugiat nisl pretium fusce. Ultricies integer quis auctor elit sed vulputate mi sit. Quis blandit turpis cursus in hac habitasse platea. Quis eleifend quam adipiscing vitae. Odio morbi quis commodo odio. Sagittis nisl rhoncus mattis rhoncus urna neque. Tincidunt arcu non sodales neque sodales. Lobortis mattis aliquam faucibus purus in massa. Leo duis ut diam quam nulla porttitor." +
    "Feugiat in fermentum posuere urna nec tincidunt praesent. Malesuada pellentesque elit eget gravida cum sociis. Nisi lacus sed viverra tellus. Egestas quis ipsum suspendisse ultrices gravida dictum fusce ut placerat. Tellus mauris a diam maecenas. Maecenas ultricies mi eget mauris pharetra et. Sem et tortor consequat id porta nibh venenatis cras sed. Tincidunt dui ut ornare lectus sit amet est placerat. Sit amet cursus sit amet dictum sit amet. Dignissim diam quis enim lobortis. Tristique sollicitudin nibh sit amet. In mollis nunc sed id semper risus in hendrerit gravida. Proin sed libero enim sed faucibus turpis in eu. Etiam sit amet nisl purus in mollis nunc sed. Vestibulum rhoncus est pellentesque elit. Id venenatis a condimentum vitae sapien pellentesque habitant morbi tristique. Est velit egestas dui id." +
    "Eu augue ut lectus arcu. Congue quisque egestas diam in arcu cursus euismod quis. Faucibus turpis in eu mi bibendum neque egestas congue quisque. Tincidunt arcu non sodales neque sodales. Imperdiet massa tincidunt nunc pulvinar. Justo eget magna fermentum iaculis eu non diam phasellus vestibulum. Leo vel orci porta non. Quis hendrerit dolor magna eget est lorem. Semper viverra nam libero justo laoreet sit amet. Laoreet non curabitur gravida arcu ac tortor dignissim. At urna condimentum mattis pellentesque id nibh. Porttitor leo a diam sollicitudin tempor id. Aliquet bibendum enim facilisis gravida neque convallis. Ac placerat vestibulum lectus mauris." +
    "Turpis egestas sed tempus urna. Porttitor rhoncus dolor purus non. Eu sem integer vitae justo eget magna fermentum iaculis. Tortor at auctor urna nunc id cursus metus. Sed risus ultricies tristique nulla aliquet enim tortor at auctor. Eget lorem dolor sed viverra ipsum nunc. Sagittis id consectetur purus ut faucibus pulvinar elementum integer enim. Orci ac auctor augue mauris augue neque gravida in fermentum. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. Tristique senectus et netus et malesuada fames ac turpis. Egestas integer eget aliquet nibh praesent tristique magna sit. Sit amet justo donec enim diam vulputate. Quis enim lobortis scelerisque fermentum dui. Diam quis enim lobortis scelerisque fermentum dui faucibus. Amet tellus cras adipiscing enim eu turpis egestas pretium. Elementum curabitur vitae nunc sed velit dignissim sodales. Consequat ac felis donec et odio pellentesque. Sed id semper risus in hendrerit gravida rutrum quisque. Tortor pretium viverra suspendisse potenti nullam ac tortor vitae." +
    "Elit pellentesque habitant morbi tristique. Pretium viverra suspendisse potenti nullam ac tortor vitae purus. Enim eu turpis egestas pretium aenean. Morbi tristique senectus et netus et. Interdum varius sit amet mattis vulputate enim. Elementum integer enim neque volutpat ac tincidunt vitae semper. Aenean pharetra magna ac placerat vestibulum lectus mauris ultrices. Iaculis urna id volutpat lacus laoreet non curabitur. Tincidunt praesent semper feugiat nibh sed. Suspendisse faucibus interdum posuere lorem ipsum dolor sit.",
]
  .join(" ")
  .replace(/[.,]/g, "")
  .replace(/\s+/g, " ")
  .toLowerCase()
  .split(" ");
const MESSAGE_END = [
  ...times(6, "."),
  ...times(3, "!"),
  ...times(2, "?"),
  ...times(2, ""),
  "...",
  "!?!",
  "?!",
];
const WORD_SPACING_CAPITAL = [...times(2, ";"), ...MESSAGE_END].map(
  (it) => `${it} `
);

export function genArray(count: number): number[] {
  return [...Array.from(Array(count)).keys()];
}

function times<T>(count: number, value: T): T[] {
  return genArray(count).map(() => value);
}

export function genInt(cap: number): number;
export function genInt(min: number, max: number): number;
export function genInt(min: number, max?: number): number {
  if (max === undefined) {
    [min, max] = [0, min - 1];
  }
  return (min + Math.random() * (max - min + 1)) | 0;
}

export function sampleOne<T>(arr: T[]): T | undefined {
  return arr[genInt(arr.length)];
}

export function genString(
  length = genInt(3, 20),
  radix = 36,
  radixOffset = 0
): string {
  const range = radix - radixOffset;
  let result = "";
  while (--length >= 0) {
    result += (radixOffset + genInt(range)).toString(radix);
  }
  return result;
}

export function genHexString(length = genInt(3, 20)): string {
  return genString(length, 16);
}

export function genAlphaString(length = genInt(3, 20)): string {
  return genString(length, 36, 10);
}

export function genMessage(wordCount = genInt(12, 60)): string {
  const words = genArray(wordCount)
    .map(() => sampleOne(WORDS)!)
    .map((word) => (Math.random() > 0.92 ? capitalCase(word) : word));
  let result = "";
  for (let word of words) {
    if (!result || Math.random() < 0.8) {
      if (result) {
        result += " ";
      } else {
        if (Math.random() > 0.1) {
          word = capitalCase(word);
        }
      }
      result += word;
    } else if (Math.random() < 0.5) {
      result += ", " + word;
    } else {
      if (Math.random() > 0.1) {
        word = capitalCase(word);
      }
      result += sampleOne(WORD_SPACING_CAPITAL) + " " + word;
    }
  }
  return result + sampleOne(MESSAGE_END);
}

export function genSwitch<T>(first: () => T, ...rest: (() => T)[]): T;
export function genSwitch<T>(...factories: (() => T)[]): T {
  return sampleOne(factories)!();
}

export function genName(): string {
  const components = genArray(genInt(1, 3))
    .map(() => genAlphaString(genInt(3, 14)))
    .map(capitalCase);
  return [
    components
      .slice(0, components.length - 1)
      .join(Math.random() < 0.5 ? "-" : " "),
    components.slice(components.length - 1),
  ]
    .filter(identity)
    .join(" ");
}

export function genFakeUUID(): string {
  return `${genHexString(8)}-${genHexString(4)}-${genHexString(
    4
  )}-${genHexString(4)}-${genHexString(12)}`;
}
