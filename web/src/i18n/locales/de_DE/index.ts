import type { Translations } from "@/i18n/constants";
import type { ChatRoom } from "@/modules/chat/chat.service";
import { capitalCase } from "@/utils/string";

export const translations: Translations = {
  chat: {
    latestActivity: "Letzte Aktivität",
    roomTitle: ({ profileForeign: { name, age } }: ChatRoom) =>
      `Konversation mit ${name} (${age})`,
    submit: "Senden",
    message: "Nachricht",
  },
  title: "FOSS Dating",
  navigation: {
    title: "Menü",
    people: "Personen",
    likes: "Anfragen",
    chat: "Nachrichten",
    profile: "Profil",
    account: "Account",
    logout: "Ausloggen",
    support: "Hilfe",
    extendedMenu: "erweitertes Menü",
  },
  languageSwitch: "Sprachauswahl",
  legal: {
    imprint: "Impressum",
    dataProtection: "Datenschutz",
  },
  profile: {
    title: "Profil",
    portrait: "Porträt",
    selection: "Profilauswahl",
    age: "Age",
    name: "Name",
  },
  people: {
    noneFound: "Keine neuen Personen entsprechen Ihren Filtern.",
  },
  relation: {
    isLikedBy: "wurde angefragt von",
    likes: "hat angefragt",
    matchesWith: "ist ein Treffer mit",
  },
  toggle: (name: string, expanded: boolean) =>
    `${capitalCase(name)} ${expanded ? "verbergen" : "anzeigen"}`,
};
