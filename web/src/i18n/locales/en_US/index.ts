import type { ChatRoom } from "@/modules/chat/chat.service";

export const translations = {
  chat: {
    latestActivity: "Latest activity",
    roomTitle: ({ profileForeign: { name, age } }: ChatRoom) =>
      `Conversation with ${name} (${age})`,
    submit: "Submit",
    message: "Message",
  },
  title: "FOSS Dating",
  navigation: {
    title: "Menu",
    people: "People",
    likes: "Likes",
    chat: "Chat",
    profile: "Profile",
    account: "Account",
    logout: "Logout",
    support: "Support",
    extendedMenu: "extended menu",
  },
  languageSwitch: "Language switch",
  legal: {
    imprint: "Imprint",
    dataProtection: "Data Protection",
  },
  profile: {
    title: "Profile",
    portrait: "Portrait",
    selection: "Profile Selection",
    age: "Age",
    name: "Name",
  },
  people: {
    noneFound: "No new people match your filters.",
  },
  relation: {
    isLikedBy: "is liked by",
    likes: "likes",
    matchesWith: "matches with",
  },
  toggle: (name: string, expanded: boolean) =>
    `${expanded ? "Collapse" : "Expand"} ${name}`,
};
