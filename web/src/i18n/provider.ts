import { ISO_639_CODE, Locale, LOCALES, Translations } from "@/i18n/constants";
import { AsyncProvider } from "@/utils/provider";
import {
  inject,
  provide,
  reactive,
  readonly,
  ref,
  Ref,
  watchEffect,
} from "vue";
import { CONFIG } from "@/utils/config";

type Primitive = string | number;

export interface LocalizedSingle<T = Primitive> {
  locale: Locale;
  content: T;
}

export type LocalizedMany<T = Primitive> = Partial<Record<Locale, T>>;

export type Localized<T = Primitive> = LocalizedSingle<T> | LocalizedMany<T>;

function isLocalizedSingle<T>(
  value: Localized<T>
): value is LocalizedSingle<T> {
  return Reflect.has(value, "locale");
}

interface I18nState {
  locale: Locale;
  translations: Translations;

  updateLocale: (locale: Locale) => Promise<void>;
  localeOf: (value: Localized) => Locale | null;
  localize: (value: Localized) => string;
  langCodeOf: (value: Localized) => string | null;
}

const SYMBOL = Symbol("provide::i18n");
const SYMBOL_TRANSLATIONS = Symbol("provide::i18n:translations");

let state!: I18nState;
let stateTranslations!: Ref<Translations>;

export const i18nProvider: AsyncProvider = async () => {
  const locale = Locale.EN_US; // todo detect based on browser / localstorage
  const { translations } = await import(`./locales/${locale}`);

  return () => {
    state = reactive({
      locale,
      translations,

      async updateLocale(locale: Locale) {
        const { translations } = await import(`./locales/${locale}`);
        state.locale = locale;
        state.translations = translations;
        stateTranslations.value = translations;
      },
      localeOf(value: Localized): Locale | null {
        if (isLocalizedSingle(value)) {
          return value.locale;
        }
        const activeLocale = state.locale;
        if (Reflect.has(value, activeLocale)) {
          return activeLocale;
        }
        for (const locale of LOCALES) {
          if (Reflect.has(value, locale)) {
            return locale;
          }
        }
        console.error("Missing Translation within", value);
        return null;
      },
      localize(value: Localized) {
        if (isLocalizedSingle(value)) {
          return value.content.toString();
        }
        const locale = state.localeOf(value);
        return locale
          ? value[locale]!.toString()
          : CONFIG.isDev
          ? "{{Missing Translation}}"
          : "";
      },
      langCodeOf(value: Localized) {
        const locale = state.localeOf(value);
        return locale ? ISO_639_CODE[locale] : null;
      },
    });
    stateTranslations = ref(translations);
    provide(SYMBOL, readonly(state));
    provide(SYMBOL_TRANSLATIONS, readonly(stateTranslations));

    watchEffect((onInvalidate) => {
      document.body.setAttribute("lang", ISO_639_CODE[state.locale]);
      onInvalidate(() => document.body.removeAttribute("lang"));
    });
  };
};

export function injectI18n() {
  return inject<I18nState>(SYMBOL)!;
}

export function injectTranslations() {
  return inject<Ref<Translations>>(SYMBOL_TRANSLATIONS)!;
}
