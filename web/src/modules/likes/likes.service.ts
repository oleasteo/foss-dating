import {
  ProfileForeignSlim,
  ProfileSelf,
} from "@/modules/profile/profile.service";
import { getMockLikes } from "@/@mock/logged-in";
import { groupBy, mapValues } from "@/utils/object";

export interface Like {
  profileSelf: ProfileSelf;
  profileForeign: ProfileForeignSlim;
  inbound: boolean;
  outbound: boolean;
  updated: string; // YYYY-MM-DD
  new: boolean;
}

export type LikeDTO = Like;

export interface LikeGroup {
  date: Date;
  likes: Like[];
}

export async function fetchLikes(): Promise<Record<string, LikeGroup>> {
  // Likes are expected to be ordered by last activity
  const groups = groupBy(await getMockLikes(), (it) => it.updated);
  return mapValues(
    groups,
    (likes): LikeGroup => ({
      date: new Date(likes[0].updated),
      likes,
    })
  );
}
