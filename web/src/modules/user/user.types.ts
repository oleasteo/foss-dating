import { ProfileSelf } from "@/modules/profile/profile.service";

export enum Role {
  USER = "user",
}

interface UserBase {
  loggedIn: boolean;
}

interface GuestUser extends UserBase {
  loggedIn: false;
}

export interface LoggedInUser extends UserBase {
  loggedIn: true;
  roles: Role[];
  profiles: ProfileSelf[];
}

export type LoggedInUserDTO = LoggedInUser;

export type User = GuestUser | LoggedInUser;
