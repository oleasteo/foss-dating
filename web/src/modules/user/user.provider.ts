import { reactive, provide, inject, readonly } from "vue";
import { fetchUser } from "@/modules/user/user.service";
import { User } from "@/modules/user/user.types";
import { AsyncProvider } from "@/utils/provider";

interface UserProvide<T extends User = User> {
  user: T;
}

const SYMBOL = Symbol("provide::user");

let state: UserProvide;

export const userProvider: AsyncProvider = async () => {
  const user = await fetchUser();

  return () => {
    state = reactive({ user });
    provide(SYMBOL, readonly(state));
  };
};

export function injectUser<T extends User = User>() {
  return inject<UserProvide<T>>(SYMBOL)!;
}
