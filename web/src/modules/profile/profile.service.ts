import { getMockProfilePreviews } from "@/@mock/logged-in";
import { LocalizedMany } from "@/i18n/provider";

interface ProfileAttribute {
  label: LocalizedMany;
  value: LocalizedMany;
}

type ProfileSection = ProfileAttribute;

export interface ProfileSelf {
  id: string;
  label: string;
}

export interface CommonProfileData {
  id: string;
  name?: string;
  age?: number;
}

export interface ProfileForeignSlim extends CommonProfileData {
  picture?: string;
}

export enum ProfilePreviewLayout {
  SIMPLE = "simple",
}

interface ProfilePreviewBase extends CommonProfileData {
  layout: ProfilePreviewLayout;
}

export interface ProfilePreviewSimple extends ProfilePreviewBase {
  layout: ProfilePreviewLayout.SIMPLE;
  picture?: string;
  attributes: ProfileAttribute[];
  section?: ProfileSection;
}

export type ProfilePreview = ProfilePreviewSimple;
export type ProfilePreviewDTO = ProfilePreview;

export function getLastActiveProfileId(
  profiles: ProfileSelf[]
): ProfileSelf["id"] {
  // todo fetch id from local storage; return first ID as fallback
  return profiles[0].id;
}

export function setLastActiveProfileId(id: ProfileSelf["id"]) {
  // todo store id to local storage
}

export async function fetchProfilePreviews(
  profile: ProfileSelf
): Promise<ProfilePreview[]> {
  return await getMockProfilePreviews(profile);
}
