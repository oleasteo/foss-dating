import { useLoading } from "@/modules/common/hooks/loading";
import { flushPromises } from "@vue/test-utils";
import {
  createPromise,
  LOADING_BUSY_VOID,
  resolvedState,
  Trigger,
} from "@/modules/common/hooks/loading/test-utils";

describe("useLoading", () => {
  jest.useFakeTimers();

  it.each([true, false])(
    "should properly handle success=%s single request flow",
    async (success) => {
      const trigger: Trigger = { id: 0, success, timeout: 1000 };
      const loading = useLoading(createPromise(trigger));

      expect(loading.state.value).toEqual(LOADING_BUSY_VOID);

      jest.advanceTimersByTime(500);
      await flushPromises();

      expect(loading.state.value).toEqual(LOADING_BUSY_VOID);

      jest.advanceTimersByTime(500);
      await flushPromises();

      expect(loading.state.value).toEqual(resolvedState(trigger));
    }
  );

  it("should reflect rejected promises", async () => {
    const trigger: Trigger = { id: 0, success: false, timeout: 1000 };
    const loading = useLoading(createPromise(trigger));
    jest.advanceTimersByTime(1000);
    await flushPromises();

    expect(loading.state.value).toEqual(resolvedState(trigger));
  });

  it.each([
    [
      { id: 0, success: true, timeout: 500 },
      { id: 1, success: true, timeout: 1000 },
    ],
    [
      { id: 0, success: true, timeout: 1000 },
      { id: 1, success: true, timeout: 500 },
    ],
    [
      { id: 0, success: false, timeout: 500 },
      { id: 1, success: true, timeout: 1000 },
    ],
    [
      { id: 0, success: true, timeout: 1000 },
      { id: 1, success: false, timeout: 500 },
    ],
  ] as [Trigger, Trigger][])(
    "should not update for outraced value",
    async (initialTrigger: Trigger, raceTrigger: Trigger) => {
      const loading = useLoading(createPromise(initialTrigger));
      await flushPromises();
      loading.next(createPromise(raceTrigger));
      await flushPromises();

      const finalState = resolvedState(raceTrigger);

      jest.advanceTimersByTime(500);
      await flushPromises();
      if (raceTrigger.timeout > 500) {
        expect(loading.state.value).toEqual(LOADING_BUSY_VOID);
      } else {
        expect(loading.state.value).toEqual(finalState);
      }

      jest.advanceTimersByTime(500);
      await flushPromises();
      expect(loading.state.value).toEqual(finalState);
    }
  );

  it("should recover from rejected value promises", async () => {
    const trigger0: Trigger = { id: 0, success: false, timeout: 500 };
    const loading = useLoading(createPromise(trigger0));
    jest.advanceTimersByTime(500);
    await flushPromises();

    expect(loading.state.value).toEqual(resolvedState(trigger0));

    const trigger1: Trigger = { id: 1, success: true, timeout: 500 };
    loading.next(createPromise(trigger1));
    await flushPromises();
    jest.advanceTimersByTime(500);
    await flushPromises();

    expect(loading.state.value).toEqual(resolvedState(trigger1));
  });

  it("should cancel on outrace", async () => {
    const trigger0: Trigger = {
      id: 0,
      success: true,
      timeout: 1000,
      onCancel: jest.fn(),
    };
    const loading = useLoading(createPromise(trigger0));
    jest.advanceTimersByTime(500);
    await flushPromises();

    expect(loading.state.value).toEqual(LOADING_BUSY_VOID);

    const trigger1 = {
      id: 1,
      success: false,
      timeout: 1000,
      onCancel: jest.fn(),
    };
    loading.next(createPromise(trigger1));
    await flushPromises();

    expect(trigger0.onCancel).toHaveBeenCalled();

    jest.advanceTimersByTime(1000);
    await flushPromises();
    expect(trigger1.onCancel).not.toHaveBeenCalled();
  });

  it.each([true, false])(
    "should hold previous value while loading",
    async (success) => {
      const trigger0: Trigger = { id: 0, success, timeout: 500 };
      const loading = useLoading(createPromise(trigger0));
      jest.advanceTimersByTime(500);
      await flushPromises();

      expect(loading.state.value).toEqual(resolvedState(trigger0));

      loading.next(createPromise({ id: 1, success: true, timeout: 500 }));
      await flushPromises();

      expect(loading.state.value).toEqual({
        ...resolvedState(trigger0),
        loading: true,
        resolved: true,
      });
    }
  );
});
