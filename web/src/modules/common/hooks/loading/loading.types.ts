interface LoadingBase<T = never, E = never> {
  loading: boolean;
  succeeded: boolean;
  resolved: boolean;
  predicted: boolean;

  value: T | null;
  error: E | null;
}

export interface LoadingVoid extends LoadingBase {
  loading: false;
  succeeded: false;
  resolved: false;
  predicted: false;
}
export interface LoadingBusyVoid extends LoadingBase {
  loading: true;
  succeeded: false;
  resolved: false;
  predicted: false;
}
export interface LoadingBusyValue<T> extends LoadingBase<T> {
  loading: true;
  succeeded: true;
  resolved: true;
  predicted: false;
}
export interface LoadingBusyPredicted<T> extends LoadingBase<T> {
  loading: true;
  succeeded: true;
  resolved: true;
  predicted: true;
}
export interface LoadingBusyError<E> extends LoadingBase<never, E> {
  loading: true;
  succeeded: false;
  resolved: true;
  predicted: false;
}
export interface LoadingSuccess<T> extends LoadingBase<T> {
  loading: false;
  succeeded: true;
  resolved: true;
  predicted: false;
}
export interface LoadingError<E> extends LoadingBase<never, E> {
  loading: false;
  succeeded: false;
  resolved: true;
  predicted: false;
}

export type LoadingBusy<T = never, E = never> =
  | LoadingBusyVoid
  | LoadingBusyValue<T>
  | LoadingBusyPredicted<T>
  | LoadingBusyError<E>;

export type Loading<T = never, E = never> =
  | LoadingVoid
  | LoadingBusy<T, E>
  | LoadingSuccess<T>
  | LoadingError<E>;
