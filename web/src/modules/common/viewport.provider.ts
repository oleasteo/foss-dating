import {
  inject,
  provide,
  readonly,
  ref,
  Ref,
  onMounted,
  onUnmounted,
} from "vue";

export enum Viewport {
  MOBILE = "mobile",
  XS = "xs",
  SM = "sm",
  MD = "md",
  LG = "lg",
  XL = "xl",
  XXL = "xxl",
}

const VIEWPORTS = Object.values(Viewport);

export const VIEWPORT_CAP: { [Key in Viewport]: number } = {
  [Viewport.MOBILE]: 480,
  [Viewport.XS]: 640,
  [Viewport.SM]: 860,
  [Viewport.MD]: 1024,
  [Viewport.LG]: 1200,
  [Viewport.XL]: 1680,
  [Viewport.XXL]: Number.POSITIVE_INFINITY,
};

const SYMBOL = Symbol("provide::viewport");
const SYMBOL_WIDTH = Symbol("provide::viewport:width");

let viewport!: Ref<Viewport>;
let width!: Ref<number>;

export function provideViewport() {
  viewport = ref(findViewport());
  width = ref(window.innerWidth);

  provide(SYMBOL, readonly(viewport));
  provide(SYMBOL_WIDTH, readonly(width));

  onMounted(() => window.addEventListener("resize", resizeListener));
  onUnmounted(() => window.removeEventListener("resize", resizeListener));

  function resizeListener() {
    viewport.value = findViewport();
  }
}

export function injectViewport() {
  return inject<Ref<Viewport>>(SYMBOL)!;
}

export function injectWindowWidth() {
  return inject<Ref<number>>(SYMBOL_WIDTH)!;
}

export function isBelow(vp0: Viewport, vp1: Viewport) {
  return VIEWPORT_CAP[vp0] < VIEWPORT_CAP[vp1];
}

function findViewport(windowWidth = window.innerWidth): Viewport {
  return VIEWPORTS.find((it) => VIEWPORT_CAP[it] > windowWidth)!;
}
