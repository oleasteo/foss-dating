export interface PromiseAndCancel<T> {
  promise: Promise<T>;
  cancel: () => void;
}

export type PromiseMayCancel<T> = Promise<T> | PromiseAndCancel<T>;

export function getPromise<T>(pmc: PromiseMayCancel<T>): Promise<T> {
  return pmc instanceof Promise ? pmc : pmc.promise;
}

export function isCancelable<T>(
  pmc: PromiseMayCancel<T>
): pmc is PromiseAndCancel<T> {
  return !(pmc instanceof Promise);
}

export async function delay(ms: number): Promise<void> {
  await new Promise((resolve) => setTimeout(resolve, ms));
}
