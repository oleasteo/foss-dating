export function groupBy<T>(
  list: T[],
  iteratee: (it: T, idx: number) => string
): Record<string, T[]> {
  const result: Record<string, T[]> = {};
  for (let i = 0; i < list.length; i++) {
    const current = list[i];
    const key = iteratee(current, i);
    if (Reflect.has(result, key)) {
      result[key].push(current);
    } else {
      result[key] = [current];
    }
  }
  return result;
}

export function mapValues<
  NewValue,
  Value,
  Key extends string | number = string
>(
  obj: Record<Key, Value>,
  iteratee: (it: Value, key: Key) => NewValue
): Record<Key, NewValue> {
  const result: Record<Key, NewValue> = {} as Record<Key, NewValue>;
  for (const key in obj) {
    result[key] = iteratee(obj[key], key);
  }
  return result;
}
