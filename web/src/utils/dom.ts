import { Booly } from "@/utils/types";

export function findClosest(
  el: Element,
  iteratee: (el: Element) => Booly
): Element | null {
  let current: Element | null = el;
  while (current !== null) {
    if (iteratee(current)) {
      return current;
    }
    current = current.parentElement;
  }
  return null;
}

export function isEqualOrAncestorOf(parent: Element, el: Element): boolean {
  return !!findClosest(el, (it) => it === parent);
}
